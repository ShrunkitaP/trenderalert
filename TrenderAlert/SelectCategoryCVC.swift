//
//  SelectCategoryCVC.swift
//  TrenderAlert
//
//  Created by HPL on 12/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class SelectCategoryCVC: UICollectionViewCell {
    
    @IBOutlet weak var img_check: UIImageView!
    @IBOutlet weak var lbl_CategoryName: UILabel!
    
    @IBOutlet weak var img_CategoryImage: UIImageView!
}

//
//  AllTrendCollectionViewCell.swift
//  TrenderAlert
//
//  Created by Admin on 24/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class AllTrendCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var trendTable: UITableView!
    
    @IBOutlet weak var trendsNotFound: UILabel!
    
}

//
//  SelectedTrendsCollectionViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 17/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class SelectedTrendsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var trendName: UILabel!
    @IBOutlet weak var selectedImg: UIImageView!
    
}

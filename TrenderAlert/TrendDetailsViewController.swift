//
//  TrendDetailsViewController.swift
//  TrenderAlert
//
//  Created by OSP LABS on 17/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit
import KLCPopup

class TrendDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var commentsTableView: UITableView!
    
    var activeInactive = UIView()
    var popup = KLCPopup()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.commentsTableView.estimatedRowHeight = 78
        self.commentsTableView.rowHeight = UITableViewAutomaticDimension
        self.commentsTableView.register(UINib(nibName: "CommentsTableViewCell", bundle: nil) , forCellReuseIdentifier: "CommentsTableViewCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsTableViewCell", for: indexPath) as! CommentsTableViewCell
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = Bundle.main.loadNibNamed("FooterView", owner: self, options: nil)?[0] as! UIView
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 80
    }

    @IBAction func activeInactive(_ sender: UIButton) {
        activeInactive = Bundle.main.loadNibNamed("ActiveView", owner: self, options: nil)?[0] as! UIView
        popup = KLCPopup(contentView: activeInactive , showType: .bounceIn, dismissType: .bounceOut, maskType: .dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        popup.show()
      
         var activeInactiveStatus = activeInactive.viewWithTag(1) as! UILabel
        
        var activeInactiveButton = activeInactive.viewWithTag(2) as! UIButton
        
//        activeInactiveButton.addTarget(self, action: #selector(TrendDetailsViewController.cancelButton), for: .touchUpInside)
        
        let cancelButton = activeInactive.viewWithTag(3) as! UIButton
        let okayButton = activeInactive.viewWithTag(4) as! UIButton
        cancelButton.addTarget(self, action: #selector(TrendDetailsViewController.cancelButton), for: .touchUpInside)
        okayButton.addTarget(self, action: #selector(TrendDetailsViewController.cancelButton), for: .touchUpInside)
        if activeInactiveStatus.text == "Active"{
            activeInactiveButton.imageView?.image = UIImage(named: "img_SwitchOn")
        }else{
            activeInactiveButton.imageView?.image = UIImage(named: "img_SwitchOFF")
        }
    }
   
    func cancelButton(){
        popup.dismissPresentingPopup()
    }
    
    func CheckActiveInactiveStatus(){
        
    }
}

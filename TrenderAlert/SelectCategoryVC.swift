//
//  SelectCategoryVC.swift
//  TrenderAlert
//
//  Created by HPL on 12/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class SelectCategoryVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var selectCategoryCollectnView: UICollectionView!
    var CategoryNamesArray = ["Music", "Sports","Science","Politics", "Entertainment", "Personal","Business","New Year"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

            selectCategoryCollectnView.allowsMultipleSelection = true
        selectCategoryCollectnView.backgroundColor = UIColor.white
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectCategoryCell", for: indexPath) as! SelectCategoryCVC
        cell.lbl_CategoryName.text = CategoryNamesArray[indexPath.row]
        cell.img_CategoryImage.image = UIImage(named: CategoryNamesArray[indexPath.row])
        if cell.isSelected{
            cell.img_check.image = UIImage(named: "img_CategoryCheck")
            cell.img_CategoryImage.layer.borderWidth = 2
            cell.img_CategoryImage.layer.borderColor = UIColor(red:250/255.0, green:225/255.0, blue:21/255.0, alpha: 1.0).cgColor
        }
        else
        {
            cell.img_check.image = nil
            cell.img_CategoryImage.layer.borderWidth = 0
            //cell.img_CategoryImage.layer.borderColor = UIColor.clear.cgColor
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         let cell = collectionView.cellForItem(at: indexPath) as! SelectCategoryCVC
        if cell.isSelected
        {
            cell.img_check.image = UIImage(named: "img_CategoryCheck")
            cell.img_CategoryImage.layer.borderWidth = 2
            cell.img_CategoryImage.layer.borderColor = UIColor(red:250/255.0, green:225/255.0, blue:21/255.0, alpha: 1.0).cgColor
        }
        else
        {
            cell.img_check.image = nil
            cell.img_CategoryImage.layer.borderWidth = 0
            //cell.img_CategoryImage.layer.borderColor = UIColor.clear.cgColor
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath) as! SelectCategoryCVC
        cell.img_check.image = nil
        cell.img_CategoryImage.layer.borderWidth = 0
        //cell.img_CategoryImage.layer.borderColor = UIColor.clear.cgColor
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.selectCategoryCollectnView.frame.width / 2)  , height: (selectCategoryCollectnView.frame.width / 2))
    }
}

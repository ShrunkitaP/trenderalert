//
//  AllTrendsViewController.swift
//  TrenderAlert
//
//  Created by Admin on 24/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class AllTrendsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var news: UIButton!
    @IBOutlet weak var myTrends: UIButton!
    @IBOutlet weak var allTrend: UIButton!
    @IBOutlet weak var animatedView: UIView!
    @IBOutlet weak var allButtonsView: UIView!
    @IBOutlet weak var trendTableView: UITableView!
    @IBOutlet weak var collectionTrend: UICollectionView!
    var scrollAnimateFlag = true
    var table = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionTrend.register(UINib(nibName: "AllTrendsAnimatedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AllTrendsAnimatedCollectionViewCell")
//        self.table.register(UINib(nibName: "TrendTableViewCell", bundle: nil), forCellWithReuseIdentifier: "TrendTableViewCell")
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func trendScrollBUttons(_ sender: UIButton) {
        let indexpath : IndexPath = IndexPath(item: sender.tag, section: 0)
        self.collectionTrend.scrollToItem(at: indexpath , at: .left, animated: true)
    }
    
    // Collection VIew Delegate methods

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllTrendsAnimatedCollectionViewCell", for: indexPath) as! AllTrendsAnimatedCollectionViewCell
//        switch indexPath.item
//        {
//        case 0:
//            cell.trendTable.register(UINib(nibName: "TrendTableViewCell", bundle: nil) , forCellReuseIdentifier: "TrendTableViewCell")
//            cell.trendsNotFound.isHidden = true
//            break
//        case 1:
//            cell.trendTable.register(UINib(nibName: "TrendTableViewCell", bundle: nil) , forCellReuseIdentifier: "TrendTableViewCell")
//            cell.trendsNotFound.isHidden = true
//            break
//        case 3:
//            cell.trendTable.register(UINib(nibName: "TrendTableViewCell", bundle: nil) , forCellReuseIdentifier: "TrendTableViewCell")
//            cell.trendsNotFound.isHidden = true
//            break
//        default:
//            break
//        }
        cell.collectionTableView.register(UINib(nibName: "TrendTableViewCell", bundle: nil) , forCellReuseIdentifier: "TrendTableViewCell")
        //cell.frame.size = self.view.frame.size
        cell.collectionTableView.delegate = self
        cell.collectionTableView.dataSource = self
        cell.collectionTableView.reloadData()
        cell.collectionTableView.layoutIfNeeded()
        cell.collectionTableView.layoutSubviews()
        cell.layoutSubviews()
        //cell.collectionTableView.layoutIfNeeded()
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
   
         return CGSize(width: self.collectionTrend.frame.size.width, height: self.collectionTrend.frame.size.height)
    }
    
    
    
//
    // Table View Delegate methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 126
    }

//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrendTableViewCell", for: indexPath) as! TrendTableViewCell
        cell.layoutIfNeeded()
        return cell
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        self.view.endEditing(true)
        if scrollView == self.collectionTrend  {
            self.animatedView.frame.origin.x = scrollView.contentOffset.x / 4
        }
    }
    

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if  scrollView == self.collectionTrend
        {
            print(scrollView.contentOffset.x)
            var scrollPositon : Int
            {
                var i = Int()
                switch scrollView.contentOffset.x
                {
                case self.collectionTrend.frame.width * 0:
                    print(self.collectionTrend.frame.width * 0)
                    i = 0
                case self.collectionTrend.frame.width * 1:
                    i = 1
                case self.collectionTrend.frame.width * 2:
                    i = 2
                    
                default:
                    break
                }
                return i
            }
            
            animateButtonUnderline(sender: scrollPositon)
            self.collectionTrend.reloadData()
        }
        

    }
    
    func animateButtonUnderline(sender:  Int) {
        //print(sender)
        switch sender {
        case 0:
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                self.animatedView.frame.origin.x = self.allTrend.frame.origin.x
            }, completion: nil)
            break
        case 1:
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                self.animatedView.frame.origin.x = self.myTrends.frame.origin.x
            }, completion: nil)
            break
        case 2:
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                self.animatedView.frame.origin.x = self.news.frame.origin.x
            }, completion: nil)
            break
        
        default:
            break
        }
    }


    
}

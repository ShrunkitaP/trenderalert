//
//  FollowerProfileFollowingViewController.swift
//  TrenderAlert
//
//  Created by HPL on 16/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class FollowerProfileFollowingViewController: UIViewController {

    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var followingTable: UITableView!
    
    var names = ["dan.shure", "mike_valera", "luiz_centenaro", "dan.shure", "John.Mathew", "mike_valera"]
    var searchNames = ["Dan Shure", "Mike Valera", "luiz Centenaro", "Dan Shure", "John Mathew", "Mike Valera"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.followingTable.register(UINib(nibName: "FollowersTableViewCell", bundle: nil) , forCellReuseIdentifier: "FollowersTableViewCell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FollowerProfileFollowingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 || indexPath.row == 3 || indexPath.row == 4  {
        let cellOne = tableView.dequeueReusableCell(withIdentifier: "FollowersTableViewCell", for: indexPath) as! FollowersTableViewCell
        
        cellOne.searchName.text = searchNames[indexPath.row]
        cellOne.name.text = names[indexPath.row]
        cellOne.followButton.titleLabel?.text = "     FOLLOW     "
            
        cellOne.followButton.backgroundColor = UIColor(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
            
        return cellOne
        }else{
            let cellTwo = tableView.dequeueReusableCell(withIdentifier: "FollowersTableViewCell", for: indexPath) as! FollowersTableViewCell
            
            cellTwo.searchName.text = searchNames[indexPath.row]
            cellTwo.name.text = names[indexPath.row]
            cellTwo.followButton.titleLabel?.text = "     FOLLOWING     "
            cellTwo.followButton.backgroundColor = UIColor(red: 251/255.0, green: 216/255.0, blue: 24/255.0, alpha: 1.0)

            return cellTwo
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
}

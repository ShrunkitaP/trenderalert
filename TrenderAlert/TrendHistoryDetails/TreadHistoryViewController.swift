//
//  TreadHistoryViewController.swift
//  TrenderAlert
//
//  Created by Admin on 22/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class TreadHistoryViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var searchTrend: UITextField!
    @IBOutlet weak var historyTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.historyTable.rowHeight = UITableViewAutomaticDimension
        self.historyTable.estimatedRowHeight = 130
        self.historyTable.register(UINib(nibName: "TrendTableViewCell", bundle: nil) , forCellReuseIdentifier: "TrendTableViewCell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrendTableViewCell", for: indexPath) as! TrendTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
  
}

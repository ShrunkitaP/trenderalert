//
//  FollowerProfileFollowersViewController.swift
//  TrenderAlert
//
//  Created by HPL on 16/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class FollowerProfileFollowersViewController: UIViewController {

    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var followersTable: UITableView!
    
    var names = ["dan.shure", "mike_valera", "luiz_centenaro", "dan.shure", "John.Mathew", "mike_valera"]
    var searchNames = ["Dan Shure", "Mike Valera", "luiz Centenaro", "Dan Shure", "John Mathew", "Mike Valera"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.followersTable.register(UINib(nibName: "FollowersTableViewCell", bundle: nil) , forCellReuseIdentifier: "FollowersTableViewCell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FollowerProfileFollowersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "FollowersTableViewCell", for: indexPath) as! FollowersTableViewCell
        
                cell.searchName.text = searchNames[indexPath.row]
        cell.name.text = names[indexPath.row]
                cell.followButton.titleLabel?.text = "     FOLLOW     "
                
            return cell
            }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
}

//
//  searchResultCollectionViewCell.swift
//  TrenderAlert
//
//  Created by Admin on 20/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class searchResultCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var trendImg: UIImageView!
    @IBOutlet weak var trendName: UILabel!
    @IBOutlet weak var trendExpiry: UILabel!
    
}

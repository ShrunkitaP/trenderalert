//
//  CategoryPopUpTableViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 16/05/17.
//  Copyright © 2017 OSP LABS. All rights reserved.
//

import UIKit

class CategoryPopUpTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryNames: UILabel!
    @IBOutlet weak var tickImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

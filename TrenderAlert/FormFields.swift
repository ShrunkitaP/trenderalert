//
//  FormFields.swift
//  TrenderAlert
//
//  Created by HPL on 10/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class FormFields: UIView {

    @IBOutlet weak var img_IconPic: UIImageView!
    
    @IBOutlet weak var lbl_PlaceHolderTxt: UILabel!
    
    @IBOutlet weak var txtFld_UserValue: UITextField!
    var view: UIView!
    @IBInspectable var image : UIImage? {
        get {
            return img_IconPic.image
        }
        set(image) {
            img_IconPic.image = image
        }
    }

    @IBInspectable var placeholder : String? {
        get {
            return txtFld_UserValue.placeholder
        }
        set(placeholder) {
            txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: placeholder!,attributes: [NSForegroundColorAttributeName: UIColor.black])
        }
    }

    @IBInspectable var text : String? {
        get {
            return lbl_PlaceHolderTxt.text
        }
        set(text) {
            lbl_PlaceHolderTxt.text = text
        }
    }

 override func draw(_ rect: CGRect)
 {
    
 }
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        //        let bundle = Bundle(for: type(of: self))
        let bundle = Bundle(for: self.classForCoder)
        //        let className = NSStringFromClass(self.classForCoder)
        let nib = UINib(nibName: "FormFields", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }

}

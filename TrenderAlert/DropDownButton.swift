//
//  DropDownButton.swift
//  TrenderAlert
//
//  Created by HPL on 10/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class DropDownButton: UIView {

    @IBOutlet weak var img_IconPic: UIImageView!
    
    @IBOutlet weak var lbl_PlaceHolderTxt: UILabel!
    
    @IBOutlet weak var lbl_TextToDisplay: UILabel!
    
    @IBOutlet weak var btn_showList: UIButton!
    var view: UIView!
    
    @IBInspectable var image : UIImage? {
        get {
            return img_IconPic.image
        }
        set(image) {
            img_IconPic.image = image
        }
    }

    @IBInspectable var text : String? {
        get {
            return lbl_PlaceHolderTxt.text
        }
        set(text) {
            lbl_PlaceHolderTxt.text = text
        }
    }

    @IBInspectable var textToDisp : String? {
        get {
            return lbl_TextToDisplay.text
        }
        set(textToDisp) {
            lbl_TextToDisplay.text = textToDisp
        }
    }

    override func draw(_ rect: CGRect)
    {
        
    }
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        //        let bundle = Bundle(for: type(of: self))
        let bundle = Bundle(for: self.classForCoder)
        //        let className = NSStringFromClass(self.classForCoder)
        let nib = UINib(nibName: "DropDownButton", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
}



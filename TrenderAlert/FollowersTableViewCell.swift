//
//  FollowersTableViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 16/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class FollowersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var searchName: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var followButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

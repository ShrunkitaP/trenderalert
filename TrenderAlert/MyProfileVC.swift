//
//  MyProfileVC.swift
//  TrenderAlert
//
//  Created by HPL on 15/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class MyProfileVC: UIViewController {

    @IBOutlet weak var view_News: UIView!
    @IBOutlet weak var view_ImFollowing: UIView!
    @IBOutlet weak var view_MyFollowers: UIView!
    @IBOutlet weak var view_TriButtons: UIView!
    @IBOutlet weak var tbl_MyProfileTableView: UITableView!
    @IBOutlet weak var img_ProfilePic: UIImageView!
    var names = ["John Newman", "Mitchell Starc", "Robin", "John Doe", "John Mathew"]
    override func viewDidLoad() {
        super.viewDidLoad()
        view_MyFollowers.backgroundColor = UIColor(red: 251/255.0, green: 216/255.0, blue: 24/255.0, alpha: 1.0)
        self.tbl_MyProfileTableView.register(UINib(nibName: "MyFollowersTableViewCell", bundle: nil) , forCellReuseIdentifier: "MyFollowersTableViewCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btn_MyFollowers(_ sender: Any) {
        
        if(!(view_MyFollowers.backgroundColor!.isEqual(UIColor(red:251/255.0, green:216/255.0, blue:24/255.0, alpha: 1.0))))
        {
            view_MyFollowers.backgroundColor = UIColor(red: 251/255.0, green: 216/255.0, blue: 24/255.0, alpha: 1.0)
            view_ImFollowing.backgroundColor = UIColor(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
            view_News.backgroundColor = UIColor(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
            tbl_MyProfileTableView.reloadData()
        }
    }

    @IBAction func btn_ImFollowing(_ sender: Any) {
        if(!(view_ImFollowing.backgroundColor!.isEqual(UIColor(red:251/255.0, green:216/255.0, blue:24/255.0, alpha: 1.0))))
        {
            view_MyFollowers.backgroundColor = UIColor(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
            view_ImFollowing.backgroundColor = UIColor(red: 251/255.0, green: 216/255.0, blue: 24/255.0, alpha: 1.0)
            view_News.backgroundColor = UIColor(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
            tbl_MyProfileTableView.reloadData()
        }

    }
    
    @IBAction func btn_News(_ sender: Any) {
        if(!(view_News.backgroundColor!.isEqual(UIColor(red:251/255.0, green:216/255.0, blue:24/255.0, alpha: 1.0))))
        {
            view_MyFollowers.backgroundColor = UIColor(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
            view_News.backgroundColor = UIColor(red: 251/255.0, green: 216/255.0, blue: 24/255.0, alpha: 1.0)
            view_ImFollowing.backgroundColor = UIColor(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
            tbl_MyProfileTableView.reloadData()
        }
    }
}

extension MyProfileVC: UITableViewDelegate, UITableViewDataSource {
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(view_MyFollowers.backgroundColor!.isEqual(UIColor(red:251/255.0, green:216/255.0, blue:24/255.0, alpha: 1.0)))
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyFollowersTableViewCell", for: indexPath) as! MyFollowersTableViewCell
            if(indexPath.row % 2 == 0)
            {
                cell.img_Line.image = UIImage(named: "img_LineMsg")
                cell.lbl_Name.text = names[indexPath.row]
                cell.btn_Following.titleLabel?.text = "FOLLOWING"
                cell.btn_Following.backgroundColor = UIColor(red: 224/255.0, green: 225/255.0, blue: 224/255.0, alpha: 1.0)
            }
            else
            {
                cell.img_Line.image = UIImage(named: "img_LineProfile")
                cell.lbl_Name.text = names[indexPath.row]
                cell.btn_Following.titleLabel?.text = "FOLLOW"
                cell.btn_Following.backgroundColor = UIColor(red: 251/255.0, green: 216/255.0, blue: 24/255.0, alpha: 1.0)
            }
            return cell
        }
        else if(view_ImFollowing.backgroundColor!.isEqual(UIColor(red:251/255.0, green:216/255.0, blue:24/255.0, alpha: 1.0)))
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyFollowersTableViewCell", for: indexPath) as! MyFollowersTableViewCell
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyFollowersTableViewCell", for: indexPath) as! MyFollowersTableViewCell
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view_TriButtons.frame.size.height
    }
    
}

//
//  MyFollowersTableViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 15/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class MyFollowersTableViewCell: UITableViewCell {

    @IBOutlet weak var img_Line: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var btn_Following: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

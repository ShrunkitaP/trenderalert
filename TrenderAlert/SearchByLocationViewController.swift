//
//  SearchByLocationViewController.swift
//  TrenderAlert
//
//  Created by Admin on 18/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit

class SearchByLocationViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , CLLocationManagerDelegate, GMSMapViewDelegate{
   // @IBOutlet weak var searchMap: MKMapView!
    @IBOutlet weak var searchMap: GMSMapView!
    @IBOutlet weak var treadingCollection: UICollectionView!
    var trendImgsArray = ["img_mapCollection1", "img_mapCollection2","img_mapCollection3", "img_mapCollection4"]
    var trendName = ["List of Caribbean music","are folks gambling like..","Stage performance","List of Sports events"]
    var latitude : Double!
    var longitude : Double!
    var location_arr : [[String:String]]!

    let position = CLLocationCoordinate2D(latitude: 10, longitude: 10)
    var gmsCamera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 10.0)
    var gmsCircle : GMSCircle!
    let selfMarker = GMSMarker()
    
    let gmsAttributes : [(Float,Double)] = [(11.0,8046.72),(10.0,16093.4),(9.0,32186.9),(8.5,48280.3),(8.0,64373.8)]
    let locationManager = CLLocationManager()
    
    var TrendsNearBy = [[String:AnyObject]]()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchMap.delegate = self
        selfMarker.icon = UIImage(named : "img_MapLogo")
        selfMarker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
    
       searchMap.camera = gmsCamera
        selfMarker.isDraggable = true
        selfMarker.map = searchMap
        gmsCircle = GMSCircle()
        
        gmsCircle.radius = gmsAttributes[0].1
        gmsCircle.position = selfMarker.position
        gmsCircle.map = searchMap
        gmsCircle.fillColor = UIColor.yellow.withAlphaComponent(0.2)
        gmsCircle.strokeColor = UIColor.black


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trendImgsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchResultCollectionViewCell", for: indexPath) as! searchResultCollectionViewCell
        cell.trendImg.image = UIImage(named: trendImgsArray[indexPath.row])
        cell.trendName.text = self.trendName[indexPath.row]
        cell.trendExpiry.text = "Expiration in 72 hours"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.treadingCollection.frame.width / 2)  , height: (treadingCollection.frame.width / 2) + 40)
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        latitude = locations.first!.coordinate.latitude
        longitude = locations.first!.coordinate.longitude
        manager.stopUpdatingLocation()
        manager.stopMonitoringSignificantLocationChanges()
        selfMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        latitude = selfMarker.position.latitude
        longitude = selfMarker.position.longitude
        
            gmsCircle.position = selfMarker.position
    }

    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker)
    {
        gmsCircle.position = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    }
    @IBAction func firstMile(_ sender: UIButton) {
        print(sender.tag)
        gmsCircle.radius = gmsAttributes[sender.tag].1
    }

}


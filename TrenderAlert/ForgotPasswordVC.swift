//
//  ForgotPasswordVC.swift
//  TrenderAlert
//
//  Created by HPL on 12/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    var prefersStatusBarHiddenFlag = true
    override var prefersStatusBarHidden: Bool {
        if prefersStatusBarHiddenFlag {
            return true
        }
        else {
            prefersStatusBarHiddenFlag = true
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.setNeedsStatusBarAppearanceUpdate()
        prefersStatusBarHiddenFlag = false
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

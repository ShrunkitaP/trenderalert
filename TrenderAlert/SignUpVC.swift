//
//  SignUpVC.swift
//  TrenderAlert
//
//  Created by HPL on 10/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit
import KLCPopup

class SignUpVC: UIViewController {

    @IBOutlet weak var view_FirstName: FormFields!
    @IBOutlet weak var view_LastName: FormFields!
    @IBOutlet weak var view_RecoveryQuestion: DropDownButton!
    var tbl_RecoveryQuestions : UITableView!
    var alertview = UIView()
    var popup : KLCPopup?
    var recoveryQuestionsArray = ["What is your pet's name?", "What is your nickname?", "What is your favourite subject?", "Your favourite game?", "Your favourite dish?"]
    var prefersStatusBarHiddenFlag = true
    override var prefersStatusBarHidden: Bool {
        if prefersStatusBarHiddenFlag {
            return true
        }
        else {
            prefersStatusBarHiddenFlag = true
            return false
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()


    view_RecoveryQuestion.btn_showList.addTarget(self, action: #selector(self.btn_ShowList(_:)), for: UIControlEvents.touchUpInside)
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.isNavigationBarHidden = true
        self.setNeedsStatusBarAppearanceUpdate()
        prefersStatusBarHiddenFlag = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btn_BackAction(_ sender: Any) {
         _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_ProfilePicSelection(_ sender: Any) {
        self.performSegue(withIdentifier: "EmailVerificationVC", sender: nil)
    }
    @IBAction func btn_SignupAction(_ sender: Any)
    {
        
        alertview = Bundle.main.loadNibNamed("ConfirmPassword", owner: self, options: nil)?[0] as! UIView
        popup = KLCPopup(contentView: alertview , showType: .bounceIn, dismissType: .bounceOut, maskType: .dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        popup!.show()
    }
    
    
      func btn_ShowList(_ sender: UIButton)
      {
            alertview = Bundle.main.loadNibNamed("RecoveryQuestionPopup", owner: self, options: nil)?[0] as! UIView
        tbl_RecoveryQuestions = (alertview.viewWithTag(1)! as! UITableView)
        popup = KLCPopup(contentView: alertview , showType: .bounceIn, dismissType: .bounceOut, maskType: .dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        popup!.show()
      }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension SignUpVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return recoveryQuestionsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        tbl_RecoveryQuestions.register(UINib(nibName: "RecoveryQuestionTVC", bundle: nil), forCellReuseIdentifier: "LoadRow")
        let cell: RecoveryQuestionTVC! = tableView.dequeueReusableCell(withIdentifier: "LoadRow",for: indexPath) as? RecoveryQuestionTVC
        cell.lbl_questionTitle.text = recoveryQuestionsArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}

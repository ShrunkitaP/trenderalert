//
//  PageLoadingViewController.swift
//  TrenderAlert
//
//  Created by OSP LABS on 17/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class PageLoadingViewController: UIViewController, UICollectionViewDelegate , UICollectionViewDataSource {

    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var selectedTrendsCollectionView: UICollectionView!
    var CategoryNamesArray = ["Music", "Sports","Science"]
    var current : Float = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedTrendsCollectionView.allowsMultipleSelection = true
        selectedTrendsCollectionView.backgroundColor = UIColor.white
        progressBar.progress = current
        self.perform(#selector(self.increaseProgress), with: nil, afterDelay: 0.3)
       // NotificationCenter.default.addObserver(self, selector: navigateToNextScreen(), name: "SelectedTrends", object: nil)
        // Do any additional setup after loading the view.
    }
    
    func increaseProgress(){
        if current < 0.99 {
            current += 0.09
            self.progressBar.progress = current
            self.perform(#selector(self.increaseProgress), with: nil, afterDelay: 0.10)
        }
    }

//    func navigateToNextScreen(){
//        current = 1
//        progressBar.progress = 1
//        self.perform(<#T##aSelector: Selector##Selector#>, with: <#T##Any?#>, afterDelay: <#T##TimeInterval#>)
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return CategoryNamesArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedTrendsCollectionViewCell", for: indexPath) as! SelectedTrendsCollectionViewCell
        cell.trendName.text = CategoryNamesArray[indexPath.row]
        cell.selectedImg.image = UIImage(named: CategoryNamesArray[indexPath.row])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.selectedTrendsCollectionView.frame.width / 2)  , height: (selectedTrendsCollectionView.frame.width / 2))
    }

    
}
